import MorseRoutes from "../components/meli-morse/routes/morseRoutes";
import ApiRoutes from "../components/frontServe/routes/apiRoutes";

export function setRouter(app) {
  new MorseRoutes(app);
  new ApiRoutes(app);
}
