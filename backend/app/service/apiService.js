import { API_URL } from "../utils/constants.js";
import { categoryParser } from "../utils/customDataParser";
import axios from "axios";
const API = axios.create({ baseURL: API_URL });

export default class ApiService {
  
  static async searchProducts(param) {
    try {
      const request = await API.get( `${API_URL}/sites/MLA/search?q=:${param}&limit=4`);
      const res = await request.data;
      return res;
    } catch (error) {
      return {
        error: true,
        message: error
      };
    }
  }

  static async searchSingleProduct(param) {
    try {
      
      const [firstResponse, secondResponse] = await Promise.all([
        API.get(`${API_URL}/items/${param}`),
        API.get(`${API_URL}/items/${param}/description`)
      ]);

      const item = firstResponse.data;
      const product_description = secondResponse.data.plain_text;
      const description_request = await API.get(`${API_URL}/categories/${item.category_id}`);
      const categories = categoryParser(description_request.data);

      const result = {
        item,
        product_description,
        categories
      };
      return result;
    } catch (error) {
      return {
        error: true,
        message: error
      };
    }
  }
}
