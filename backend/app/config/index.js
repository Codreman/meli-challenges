// todas las configuraciones del proyecto
export default {
  env: process.env.NODE_ENV || "development", // Enviroment
  port: process.env.PORT || 8000, // port
  ip: process.env.IP || "127.0.0.1", // ip server
  cors: process.env.CORS || true,
  TOKEN_SECRET: "dG9rZW51bHRyYXNlY3JldG8=", // key to unlock token
  conn: {
    host: "domain", // db domain
    user: "myuser", // user
    password: "123456", // password
    dbName: "" // db name
  },
  mail: {
    pool: true,
    host: "wo32.wiroos.host", // Host Name
    port: 465, // SMTP port
    secure: true, // upgrade later with STARTTLS
    auth: {
      user: "no-reply@example.com", // mail to send mails
      pass: "" // password
    },
    dkim: {
      domainName: "example.com", // Domain name
      keySelector: "2019",
      privateKey: "-----BEGIN PRIVATE KEY---3nMIIEvgIBADANBg..."
    },
    tls: {
      // do not fail on invalid certs
      rejectUnauthorized: false
    }
  }
};
