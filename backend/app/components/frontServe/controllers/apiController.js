import Http from "../../../utils/http";
import { priceParse } from "../../../utils/customDataParser";
import ApiService from "../../../service/apiService";
import { RESULT_HEADER } from "../../../utils/constants.js";

export default class ApiController {
  contructor(app) {
    this.app = app;
  }

  static async getProducts(req, res) {
    try {
      const criteria = req.query.search;
      // Buscamos la informacion
      const resp = await ApiService.searchProducts(criteria);

      let result = {
        ...RESULT_HEADER,
        categories: [],
        items: []
      };

      // recorremos la respuesta del servicio de meli
      if (resp.results.length) {
        resp.filters.forEach((element) => {
          if (element.name === "Categorías") {
            // armamos las categorias para breadcrumbs
            element.values[0].path_from_root.forEach((pathC) => {
              result.categories.push(pathC.name);
            });
          }
        });

        // Armamos la respuesta
        resp.results.forEach((product) => {
          const temp = {
            id: product.id,
            title: product.title,
            price: {
              currency: product.currency_id,
              amount: priceParse(product.price),
              decimals: 0
            },
            picture: product.thumbnail,
            address: product.address.state_name,
            condition: product.condition,
            free_shipping: product.shipping.free_shipping
          };

          result.items.push(temp);
        });
      }
      Http.httpOK(res, { ...result });
    } catch (error) {
      return Http.httpInternalServerError(res, error);
    }
  }

  static async getSingleProduct(req, res) {
    try {
      const criteria = req.params.id;
      console.log(criteria);
      const request = await ApiService.searchSingleProduct(criteria);

      let result = {};
      if (request.hasOwnProperty("item")) {
        result = {
          ...RESULT_HEADER,
          ...request,
          item: {
            id: request.item.id,
            title: request.item.title,
            picture: request.item.pictures[0].url,
            condition: request.item.condition === "used" ? "Usado" : "Nuevo",
            free_shipping: request.item.shipping.free_shipping,
            sold_quantity: request.item.sold_quantity,
            category_id: request.item.category_id,
            price: {
              currency: request.item.currency_id,
              amount: priceParse(request.item.price)
            }
          }
        };

        Http.httpOK(res, { ...result });
      } else {
        Http.httpBadRequest(res, "Product not Found");
      }
    } catch (error) {
      return Http.httpInternalServerError(res, error);
    }
  }
}
