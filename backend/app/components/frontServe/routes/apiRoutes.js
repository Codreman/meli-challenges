import ApiController from "../controllers/apiController";
export default app => {
  // Definimos las rutas
  app.get("/searchProducts", ApiController.getProducts);
  app.get("/searchSingleProduct/:id", ApiController.getSingleProduct);
};
