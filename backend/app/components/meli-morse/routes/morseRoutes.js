import MorseController from "../controllers/morseController";

// devolvemos las rutas de cada seccion
export default app => {
  // Definimos las rutas
  app.post("/translate/2morse", MorseController.toMorse);
  app.post("/translate/2text", MorseController.toText);
  app.post("/translate/bin2text", MorseController.toHuman);
};
