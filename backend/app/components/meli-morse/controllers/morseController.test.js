const request = require("supertest");
const { app } = require("../../index.js");
const Requester = request(app);

describe("Test endpoint MELI challenge", () => {
  it("Translate humanToMorse OK", async done => {
    // El mensaje deberia de ser:
    const menssage =
      ".... --- .-.. .-  -- . .-. -.-. .- -.. ---  .-.. .. -... .-. .  ";

    const result = await Requester.post("/translate/2morse").send({
      text: "Hola Mercado Libre"
    });

    expect(result.status).toBe(200);
    expect(result.body.data).toBe(menssage);
    done();
  });

  it("Translate MorseTohuman OK", async done => {
    // El mensaje deberia de ser:
    const menssage = "HOLA MERCADO LIBRE ";

    const result = await Requester.post("/translate/2text").send({
      text: ".... --- .-.. .-  -- . .-. -.-. .- -.. ---  .-.. .. -... .-. . "
    });

    expect(result.status).toBe(200);
    expect(result.body.data).toBe(menssage);
    done();
  });

  it("Translate toHuman OK", async done => {
    // El mensaje deberia de ser:
    const menssage = " HOLAMELI ";

    const result = await Requester.post("/translate/binMorse").send({
      text:
        "000000001101101100111000001111110001111110011111100000001110111111110111011100000001100011111100000111111001111110000000110000110111111110111011100000011011100000000000",
      type: "binary"
    });

    expect(result.status).toBe(200);
    expect(result.body.data).toBe(menssage);
    done();
  });
});
