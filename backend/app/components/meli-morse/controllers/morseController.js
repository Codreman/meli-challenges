import Http from "../../../utils/http";
import {
  convertHumanToMorse,
  convertMorseToText
} from "../../../utils/translate";
import { isMorse, isBin } from "../../../utils/validator";
import { decodeBits2Morse } from "../../../utils/binaryMorse";

export default class MorseController {
  contructor(app) {
    this.app = app;
  }

  static toMorse(req, res) {
    try {
      const { text } = req.body;
      if (typeof text !== "string" || !text.length || !text.match(/^[0-9a-zA-Z ]+$/))
        return Http.httpBadRequest(res, "Información suministrada incorrecta!");

      const morse = convertHumanToMorse(text);
      if (morse.error) {
        throw new Error(morse.message);
      }

      return Http.httpOK(res, morse);
    } catch (err) {
      return Http.httpInternalServerError(res, err.message);
    }
  }

  static toText(req, res) {
    try {
      const { text } = req.body;
      if (!text || typeof text !== "string" || !isMorse(text))
        return Http.httpBadRequest(res, "Información suministrada incorrecta!");

      const value = convertMorseToText(text);
      if (value.error) {
        throw new Error(morse.message);
      }

      return Http.httpOK(res, value);
    } catch (err) {
      return Http.httpInternalServerError(res, err.message);
    }
  }
  
  static toHuman(req, res) {
    try {
      const { text } = req.body;

      if (!text || typeof text !== "string" || !isBin(text))
        return Http.httpBadRequest(res, "Información suministrada incorrecta!");

      let human = "";
      const morse = decodeBits2Morse(text);
      if (morse.error) throw new Error(morse.message);

      human = convertMorseToText(morse);
      if (human.error) throw new Error(human.message);

      return Http.httpOK(res, human);
    } catch (err) {
      return Http.httpInternalServerError(res, err.message);
    }
  }

  static toText(req, res) {
    try {
      const { text } = req.body;
      if (!text || typeof text !== "string" || !isMorse(text))
        return Http.httpBadRequest(res, "Información suministrada incorrecta!");

      const value = convertMorseToText(text);
      if (value.error) {
        throw new Error(morse.message);
      }

      return Http.httpOK(res, value);
    } catch (err) {
      return Http.httpInternalServerError(res, err.message);
    }
  }
}
