import { RESULT, HTTP_CODES } from './constants.js';


export default class Http {

	static httpOK(response, data) {
		data = data || null;
		if (!data) {
			return httpNoContent(response);
		}

		const message  = {
			status: RESULT.SUCCESS,
			data: data,
			message: 'OK'
		}
		response.status(HTTP_CODES.OK).send(message);
	}	

	static httpNoContent(response) {
		const message = {
			status: RESULT.SUCCESS,
			message: 'No Content'
		}
		response.status(HTTP_CODES.NO_CONTENT).send(message);
	}

	static httpBadRequest(response, exception) {
		exception = exception || '';

		const message = {
			status: RESULT.FAIL,
			exception: exception ,
			message: 'Someting went wrong!'
		}

		response.status(HTTP_CODES.BAD_REQUEST).send(message);
	}

	static httpUnauthorized(response, exception) {
		exception = exception || null;

		const message = {
			status: RESULT.FAIL,
			exception: exception,
			message: 'Access Denied!'
		}

		response.status(HTTP_CODES.UNAUTHORIZED).send(message);
	}

	static httpInternalServerError(response, exception) {
		exception = exception || null;

		const message = {
			status: RESULT.ERROR,
			exception: exception,
			message: 'Oops! The server has failed!'
		}

		response.status(HTTP_CODES.INTERNAL_SERVER_ERROR).send(message);
	}
}
