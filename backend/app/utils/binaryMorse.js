const { isBin, isMorse } = require("./validator");
const { convertMorseToText } = require("./translate");
const { pulseTypes } = require("./constants");

exports.decodeBits2Morse = function(binary) {
  try {
    if (!isBin(binary))
      throw new Error("Hay errores en la informacion suministrada!");

    let decodedBits = "";
    // Obtenemos el bit separado en array
    const binaryArr = binary.match(/(0)+|(1)+/g);

    // Obtenemos los pulsos separados de 0 y 1
    const onePulses = binary.split("0");
    let ceroPulses = binary.split("1");

    // Eliminamos los primeros y ultimos ceros
    ceroPulses.shift();
    ceroPulses.pop();

    // Obtenemos el promedio
    const promedyOnePulsations = getPromedy(onePulses);
    if (promedyOnePulsations.error)
      throw new Error(promedyOnePulsations.message);

    const promeryCeroPulsations = getPromedy(ceroPulses);
    if (promeryCeroPulsations.error)
      throw new Error(promeryCeroPulsations.message);

    // Evaluamos las pulsaciones
    for (let p of binaryArr) {
      if (p.length) {
        let evt = evaluate(p, promedyOnePulsations, promeryCeroPulsations);
        switch (evt) {
          case pulseTypes.shortOne:
            decodedBits += ".";
            break;
          case pulseTypes.longOne:
            decodedBits += "-";
            break;
          case pulseTypes.longCero:
            decodedBits += " ";
            break;
          case pulseTypes.shortCero:
            decodedBits += "";
            break;
          default:
            break;
        }
      }
    }

    return decodedBits;
  } catch (err) {
    return {
      error: true,
      message: err.message
    };
  }
};

exports.translate2Human = function(morse) {
  try {
    if (!morse || !isMorse(morse))
      throw new Error("Hay errores en la informacion suministrada!");

    const humanText = convertMorseToText(morse);
    if (humanText.error) throw new Error(humanText.message);

    return humanText;
  } catch (erro) {
    return {
      error: true,
      message: err.message
    };
  }
};

function evaluate(pulse, onePromedy, ceroPromedy) {
  try {
    let response = "";
    if (pulse.match(/^[1]+$/)) {
      response =
        pulse.length < onePromedy ? pulseTypes.shortOne : pulseTypes.longOne;
    } else {
      response =
        pulse.length > ceroPromedy ? pulseTypes.longCero : pulseTypes.shortCero;
    }
    return response;
  } catch (err) {
    return {
      error: true,
      message: err.message
    };
  }
}

function getPromedy(arr) {
  try {
    if (!arr || !arr.length) return false;

    let info = [];
    arr.forEach(pulse => {
      if (pulse.length) {
        info.push(pulse.length);
      }
    });
    return Math.round(info.reduce((t, l) => t + l) / info.length);
  } catch (err) {
    return {
      error: true,
      message: err.message
    };
  }
}
