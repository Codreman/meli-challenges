// Codigos HTTP para respuestas
exports.HTTP_CODES = {
  OK: 200,
  NO_CONTENT: 204,
  BAD_REQUEST: 400,
  UNAUTHORIZED: 401,
  FORBIDEN: 403,
  INTERNAL_SERVER_ERROR: 500
};

// status de respuestas
exports.RESULT = {
  SUCCESS: "Success",
  FAIL: "Fail",
  ERROR: "Error"
};

// Diccionario de Morse
exports.MORSE_DICT = {
  ".-": "A",
  "-...": "B",
  "-.-.": "C",
  "-..": "D",
  ".": "E",
  "..-.": "F",
  "--.": "G",
  "....": "H",
  "..": "I",
  ".---": "J",
  "-.-": "K",
  ".-..": "L",
  "--": "M",
  "-.": "N",
  "---": "O",
  ".--.": "P",
  "--.-": "Q",
  ".-.": "R",
  "...": "S",
  "-": "T",
  "..-": "U",
  "...-": "V",
  ".--": "W",
  "-..-": "X",
  "-.--": "Y",
  "--..": "Z",
  "-----": "0",
  ".----": "1",
  "..---": "2",
  "...--": "3",
  "....-": "4",
  ".....": "5",
  "-....": "6",
  "--...": "7",
  "---..": "8",
  "----.": "9",
  ".-.-.-": ".",
  " ": " "
};

// Definiciones de pulsos
exports.pulseTypes = {
  shortCero: 0,
  longCero: 1,
  shortOne: 2,
  longOne: 3
};

exports.API_URL = "https://api.mercadolibre.com";

exports.RESULT_HEADER = {
  author: { name: "Christian Giusseppe", lastname: "Odreman" }
};
