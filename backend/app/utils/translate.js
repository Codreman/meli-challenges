const Const = require("./constants");

exports.convertHumanToMorse = function(text) {
  try {
    if (typeof text !== "string" || !text.length)
      throw new Error("No es un string!");

    const palabras = text.split(" ");
    let morseText = "";

    palabras.forEach(p => {
      for (let i = 0; i < p.length; i++) {
        let letter = p[i].toUpperCase() || null;
        let char =
          Object.keys(Const.MORSE_DICT)[
            Object.values(Const.MORSE_DICT).indexOf(letter)
          ] || "";
        morseText += char;
        morseText += " ";
      }
      morseText += " ";
    });

    return morseText;
  } catch (err) {
    return {
      error: true,
      message: err.message
    };
  }
};

exports.convertMorseToText = function(morse) {
  try {
    if (typeof morse !== "string" || !morse.length)
      throw new Error("hay errores con la información suministrada!");

    const letters = morse.split(" ");
    let morseText = "";

    letters.forEach(p => {
      let char =
        Object.values(Const.MORSE_DICT)[
          Object.keys(Const.MORSE_DICT).indexOf(p)
        ] || " ";
      morseText += char;
    });
    return morseText;
  } catch (err) {
    return {
      error: true,
      message: err.message
    };
  }
};
