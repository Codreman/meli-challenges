exports.isMorse = function(morse) {
  if (!morse) 
    throw new Error("hay errores con la información suministrada!");
  return morse.match(/^[.\s\- ]+$/);
};

exports.isBin = function(bin) {
  if (!bin) 
    throw new Error("hay errores con la información suministrada!");
  return bin.match(/^[0-1]+$/);
};
