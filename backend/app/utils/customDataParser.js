export const priceParse = amount => {
  let price = Math.ceil(amount);
  return price.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
};

export const categoryParser = data => {
  const categories = [];
  data.path_from_root.forEach(function(pathC) {
    categories.push(pathC.name);
  });
  return categories;
};
