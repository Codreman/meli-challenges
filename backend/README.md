### Autor Christian Odreman

Challenge Backend Mercado Libre

![N|Meli](https://http2.mlstatic.com/ui/navigation/5.3.7/mercadolibre/logo__large_plus.png)

Dependencias base:

- Nodejs 10.16

## main.js

Convierte un string en morse a texto humano.
Converte binario a código morse y de morse a texto humano.

- Forma de uso:
  Parametros para la ejeción

  - binary: tipo de lenguaje de input

  - [0,1] si es binario o [.-] en caso de morse.

Ejemplo: node main.js binary 000110000011111001000

Ejemplo: node main.js morse .... --- .-.. .- -- . .-.. ..

## api Rest

Convierte texto humano a codigo morse

Convierte código morse a humano.

Convierte binario a humano.

## Forma de uso

Mediante postman o insomnia

- Se incluye una colleccion de postman para probar.

## Transformar texto a morse

POST "http://localhost:8000/translate/2morse" {"text": "HOLA MELI"}

- Ejemplo de uso
  input

  ```JSON
  {"text": "HOLA MELI"}
  ```

  output

  ```JSON
  {
    "status": "Success",
    "data": ".... --- .-.. .-  -- . .-.. ..",
    "message": "OK"
  }
  ```

## Transformar morse a texto (separar palabras por doble espacio)

POST "http://localhost:8000//translate/2text" {"text": ".... --- .-.. .- -- . .-.. .."}

- Ejemplo de uso

  input

  ```JSON
  {"text": ".... --- .-.. .-  -- . .-.. .."}
  ```

  output

  ```JSON
  {
    "status": "Success",
    "data": "HOLA MELI ",
    "message": "OK"
  }
  ```

## Transformar binario a texto

POST "http://localhost:8000/translate/bin2text" '{
"text": "000000001101101100111000001111110001111110011111100000001110111111110111011100000001100011111100000111111001111110000000110000110111111110111011100000011011100000000000",
}'

- Ejemplo de uso

  input

  ```JSON
    {
      "text": "000000001101101100111000001111110001111110011111100000001110111111110111011100000001100011111100000111111001111110000000110000110111111110111011100000011011100000000000",
    }
  ```

  output

  ```JSON
  {
    "status": "Success",
    "data": " HOLAMELI ",
    "message": "OK"
  }
  ```

# Notas

- En caso de ingresar un caracter NO valido dentro de los esperados, se disparará un Exception. En caso de estar siendo ejecutado desde la API devolverá un Bad Request (Error 400).

- Se utiliza como fin de mensaje una pausa prolongada.

## Inicio de la app

### iniciar con docker-compose

- sudo docker-compose up

### iniciar con Dockerfile

Ejecuta el builder de la imagen

- sudo docker build . < Dockerfile

Obten el id de la imagen

- sudo docker images ls

iniciar el contenedor

- sudo docker run -d -p 8000:8000 --name Node { image id or name}

Verifica que el contenedor esta corriendo

- sudo docker container ls

### iniciar en desarrollo

- yarn install o npm install
- yarn start o npm start
