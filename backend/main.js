const {
  decodeBits2Morse,
  translate2Human
} = require("./app/utils/binaryMorse");

// Obtenemos los argumentos necesarios
const args = process.argv;
if (!args[2] || !args[3]) {
  process.stdout.write("No ingreso los parametros solicitados");
  process.exit();
}

// Obetenemos los argumentos
const type = args[2];
const data = args[3];

let response = "";
// segun los datos hacemos la conversion
if (type == "binary") {
  let morse = decodeBits2Morse(data);
  response = translate2Human(morse);
} else if (type == "morse") {
  response = translate2Human(data);
} else {
  response = "Error en el tipo de texto ingresado!";
}

console.log(response);
