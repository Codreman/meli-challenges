import React from "react";
import { render } from "@testing-library/react";
import configureStore from "redux-mock-store";
import Header from "./Header";

const initialState = {
  ProductReducer: {
    search: ""
  }
};

/**
 * @desc Suite de test's
 */
describe("[Header]", () => {
  /**
   * @desc Validamos si se renderizo el contenedor.
   */
  it("[Header] When render correctl'y", () => {
    // Mockeo del store
    let mockStore = configureStore();

    // Store
    let store = mockStore(initialState);

    // Wrappeamos el componente
    let wrapper = render(<Header store={store} />, { initialState });

    // Validamos que se haya renderizado con exito.
    expect(wrapper).toMatchSnapshot();
  });
});
