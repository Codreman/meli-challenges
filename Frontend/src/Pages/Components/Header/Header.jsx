import React, { useEffect } from "react";
import { useDispatch, useSelector } from 'react-redux';
import { sanitizeParam } from "../../../Resources/middlewareUtils";
import Navbar from "../Navbar/Navbar";
import { getAllProduct } from "../../../Redux/Thunks/productsActionsCreator";
import { useCallback } from "react";

function Header(props) {
  const dispatch = useDispatch();
  const search = useSelector(state => state.ProductReducer.search);
  const { history = {} } = props;

  const updateSearch = useCallback((searching) => {
    if (searching) {
      dispatch(getAllProduct(searching));
      history.push(`/items?search=${searching}`);
    }
  }, [dispatch, history])

  const verifyUrl = useCallback((url) => {
    const searchInfo = sanitizeParam(url);
    if (search !== searchInfo) {
      updateSearch(searchInfo);
    }
  },[search, updateSearch])

  useEffect(()=> {
    const { location = {} } = history;
    if (location.search) {
      verifyUrl(location.search);
    }
  },[history, verifyUrl]);

  const redirectHome = () => {
    history.push("/");
  }

  return (
    <div className="Header">
      <Navbar
        search={ search }
        updateSearch={ search => updateSearch(search) }
        history={ history }
        redirecHome={() => redirectHome() }
      />
    </div>
  );  
}

export default Header;
