import React from "react";
import './styles/style.scss';

function Breadcrumb({ categories = [] }) {
  return (
    <ol className="bread-crumbs">
      {
        categories.map(function(item, i) {
          return <li className="bread-crumbs__item" key={i}>{item}</li>;
        }) || ""
      }
    </ol>
  );
};

export default Breadcrumb;
