import React, { useState } from "react";
import "./styles/style.scss";
import logo from "../../../Assets/Logo_ML.png";
import searchIcon from "../../../Assets/ic_Search.png";

function Navbar({ updateSearch, redirecHome }) {
  const [search, setSearch] = useState("");

  const handleKeyPress = e => {
    if (e.key === "Enter") {
      updateSearch(e.target.value);
    }
  };

  return (
    <div className="navbar">
      <div className="form-inline">
        <div className="input-group meli-input-cont">
          <span onClick={() => redirecHome()}>
            <img className="meli-logo" src={logo} alt="meli-logo" />
          </span>
          <input
            type="text"
            name="search_criteria"
            value={search}
            onChange={e => setSearch(e.target.value)}
            onKeyPress={e => handleKeyPress(e)}
            className="meli-search form-control"
            placeholder="Nunca dejes de buscar"
          />
          <span
            className="input-group-addon"
            onClick={e => updateSearch(search)}
          >
            <img src={searchIcon} alt="search" />
          </span>
        </div>
      </div>
    </div>
  );
}

export default Navbar;
