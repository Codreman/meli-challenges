import React from "react";
import "./styles/style.scss";
import Loader from "./Loader";

function PageLoading() {
  return (
    <>
      <div className="page-loading">
        <Loader />
      </div>
    </>
  );
}

export default PageLoading;
