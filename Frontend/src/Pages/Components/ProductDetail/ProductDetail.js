import React from "react";
import fs from "../../../Assets/ic_shipping.png";
import "./styles/productDetail.scss";

function ProductDetails({ product, freeShipping, description }) {
  const renderProduct = () => {
    if (product) {
      return (
        <>
          <div className="product-cont">
            <div className="row">
              <div className="photo">
                <img src={product.picture} alt="productimg" />
              </div>
              <div className="product-info">
                <p className="state-sold">
                  {product.condition} - {product.sold_quantity} Vendidos
                </p>
                <p className="product-title">
                  <b>{product.title}</b>
                </p>
                <p className="product-price">
                  $ {product.price.amount}{" "}
                  {freeShipping ? (
                    <img
                      className="free-shipping-ic"
                      src={fs}
                      alt="free-shipping"
                      title="Envio Gratis"
                    />
                  ) : (
                    ""
                  )}
                </p>
                <button className="btn-primary">Comprar</button>
              </div>
            </div>

            <div className="product-description">
              <h2>Descripcion del producto</h2>
              <p>{description}</p>
            </div>
          </div>
        </>
      );
    }
  };
  return <div className="container">{ renderProduct() }</div>;
}

export default ProductDetails;
