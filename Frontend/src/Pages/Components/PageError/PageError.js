import React from "react";
import "./styles/style.scss";

function PageError({ error }) {
  return (
    <div className="page-error">
      <div className="page-error__container">
        <h1 className="page-error__message">Oops! Ha ocurrido un Error.</h1>
        <code>{error}</code>
      </div>
    </div>
  );
};

export default PageError;
