import React from "react";
import ListItems from "./ListItems";

function List({ products = {} }) {
  const { items } = products;

  return (
    <div className="list">
      <div className="list__result">
        <ListItems products={items}/>
      </div>
    </div>
  );
};

export default List;
