import React from "react";
import { Link } from "react-router-dom";
import fs from "../../../Assets/ic_shipping.png";
import './styles/style.scss';

function ListItem({ products = [] }) {
  return (
    products.map((item, key) => (
      <li key={key}>
        <div className="product">
          <div className="product__thumb">
            <Link to={`/item/${item.id}`}>
              <img className="product__image" src={item.picture} alt="product-t" />
            </Link>
          </div>
          <div className="product__detail">
            <div className="price-location">
              <p className="product__detail-price">
                $ {item.price.amount}
                {item.free_shipping ? (
                  <img
                    className="product__free-shipping-ic"
                    src={fs}
                    alt="free-shipping"
                    title="Envio Gratis"
                  />
                ) : (
                  ""
                )}
              </p>
              <p className="product__detail-location">{item.address}</p>
            </div>
            <div className="product__title">
              <p>
                <Link className="product__link" to={`/item/${item.id}`}>{item.title}</Link>
              </p>
            </div>
          </div>
        </div>
      </li>
    ))
  );
};

export default ListItem;
