import React from "react";
import { Helmet } from "react-helmet";
import { connect } from "react-redux";
import { decodeCriteria } from "../../../Resources/middlewareUtils";

function Seo(props) {
  const renderTags = () => {
    const { error, product, search } = props.ProductReducer;

    let title = "Meli Challenge";
    let description = product.product_description
      ? product.product_description
      : "Front End Challenge for mercadolibre.com";

    if (error) {
      title = error.toString();
    }

    if (search) {
      product.hasOwnProperty("item")
        ? (title = product.item.title)
        : (title = `Buscar: ${decodeCriteria(search)}`);
    }

    return (
      <Helmet>
        <title>{title}</title>
        <meta name="description" content={description} />
      </Helmet>
    );
  };

  return <>{renderTags()}</>;  
}

const mapStateToProps = ({ ProductReducer }) => {
  return { ProductReducer };
};

export default connect(mapStateToProps)(Seo);
