import React from "react";
import { useSelector } from 'react-redux';
import Seo from "../Seo/Seo";
import Header from '../Header/Header';
import BreadCrumb from '../BreadCrumbs/breadCrumbs';

function Layout({ history, children }) {
  const categories = useSelector(state => state.ProductReducer.categories);

  return (
    <>
      <Seo />
      <Header history={history} />
      <BreadCrumb categories={ categories }/>
      { children }
    </>
  );
}

export default Layout;
