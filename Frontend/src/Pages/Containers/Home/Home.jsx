import React from "react";
import { useEffect } from "react";
import { useDispatch } from 'react-redux';
import { setCategories } from '../../../Redux/Actions/productActions';
export default function Home() {
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(setCategories([]));
  },[dispatch]);

  return (
    <>
      <div className="Home"></div>
    </>
  );
};
