import React from "react";
import { render } from "@testing-library/react";
import configureStore from "redux-mock-store";
import ProductDetail from "./ProductDetailt";

const initialState = {
  ProductReducer: {
    product: {},
    loading: false,
    error: false
  }
};

/**
 * @desc Suite de test's
 */
describe("[ProductDetailt]", () => {
  /**
   * @desc Validamos si se renderizo el contenedor.
   */
  it("[ProductDetailt] When render correctl'y", () => {
    // Mockeo del store
    let mockStore = configureStore();

    // Store
    let store = mockStore(initialState);

    // Wrappeamos el componente
    let wrapper = render(<ProductDetail store={store} />, { initialState });

    // Validamos que se haya renderizado con exito.
    expect(wrapper).toMatchSnapshot();
  });
});
