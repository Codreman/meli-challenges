import React, { useEffect } from "react";
import { useDispatch, useSelector } from 'react-redux';
import ProductDetails from "../../Components/ProductDetail/ProductDetail";
import { getOneProduct } from "../../../Redux/Thunks/productsActionsCreator";
import { useCallback } from "react";

function ProductDetailsContainer(props) {
  const dispatch = useDispatch();
  const product = useSelector(state => state.ProductReducer.product);
  const { match = {} } = props;
  const { params = {} } = match;

  const searchingData = useCallback(() => {
    if (params) {
      dispatch(getOneProduct(params));
    }
  }, [dispatch, params])

  useEffect(() => {
    searchingData();
  },[params, searchingData]);

  const renderDetails = () => {
    if (product && product.data) {
      const {
        item = {},
        categories,
        product_description
      } = product.data;
      return (
        <>
          <ProductDetails
            freeShipping={ item.free_shipping }
            categories={ categories }
            product={ item }
            description={ product_description }
          />
        </>
      );
    }
    return <div></div>;
  };  
  return <>{ renderDetails() }</>;
}

export default ProductDetailsContainer;
