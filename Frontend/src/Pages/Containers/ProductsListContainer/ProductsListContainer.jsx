import React from "react";
import { useSelector } from 'react-redux';
import List from '../../Components/ListComponent/ListComponent';
import "./styles/ProductsList.scss";

function ProductListContainer() {
  const products = useSelector(state => state.ProductReducer.products);

  return (
    <div className="container">
      <div className="row">
        <List products={ products.data } />
      </div>
    </div>
  );
}

export default ProductListContainer;