import React from 'react';
import { useSelector } from 'react-redux';
import Layout from '../../Components/Layout/Layout';
import LoadingComponent from '../../Components/Loading/Loading';
import Error from '../../Components/PageError/PageError';

function App(props) {
  const loading = useSelector(state => state.ProductReducer.loading);
  const error = useSelector(state => state.ProductReducer.error);
  
  return (
    <section className="app">
      <Layout history={props.history}>
        { props.children }
      </Layout>
      {
        loading && <LoadingComponent />
      }
      {
        error && <Error />
      }
    </section>
  );
}
export default App;