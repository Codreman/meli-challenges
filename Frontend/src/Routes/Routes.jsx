import React from "react";
import { Switch, Route, Router } from "react-router-dom";
import Home from "../Pages/Containers/Home/Home";
import ProductList from "../Pages/Containers/ProductsListContainer/ProductsListContainer";
import ProductDetail from "../Pages/Containers/ProductDetail/ProductDetail";

function Routes(props) {
  return (
    <Router history={props.history}>
      <Switch>
        <Route exact path="/home" component={Home} />
        <Route exact path="/" component={Home} />
        <Route exact path="/items" component={ProductList} />
        <Route exact path="/item/:id" component={ProductDetail} />
      </Switch>
    </Router>
  );
};
export default Routes;
