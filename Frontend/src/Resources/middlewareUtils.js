//Limpiamos el query string del url para enviar al backend solo el criterio en formato url.

export const sanitizeParam = string => {
  return string.replace("?search=", "").replace(/ /g, "+");
};

//Decodificamos y limpiamos el criterio de busqueda para que persista de manera legible en el navbar input

export const decodeCriteria = search => {
  return search ? decodeURIComponent(search.replace(/\+/g, "%20")) : "";
};
