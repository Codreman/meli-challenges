import { getProducts } from "../../Actions/productsActions";
import { sanitizeParam } from "../middlewareUtils";

const criteriaValidationMiddleware = ({
  dispatch,
  getState
}) => next => async action => {
  switch (true) {
    case action.type === "product_submit":
      const { criteria } = action;
      const new_criteria = await sanitizeParam(criteria);
      dispatch(getProducts(new_criteria));
      break;
    default:
      return next(action);
  }
};

export default criteriaValidationMiddleware;
