import axios from "axios";

export const httpGet = async (uri, options) => await axios.get(uri, options);

export const httpPost = async (uri, params, options) => await axios.post(uri, params, options);

export const httpPut = async (uri, params, options) => await axios.put(uri, params, options);

export const httpDelete = async (uri, options) => await axios.delete(uri, options);