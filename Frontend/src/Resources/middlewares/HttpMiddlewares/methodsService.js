import { httpGet, httpPost, httpPut, httpDelete } from './httpService';
import {
  buildDefaultOptions, 
  buildURLQuery,
  handleHttpResponse, 
  handleHttpError } from './http';

const accessToken = () => { 	
	const Token = sessionStorage.getItem('token') || '';
	return Token;
}

const headerOptions = buildDefaultOptions(accessToken);

export const get = async (entity_uri, getValues) => {    
  const queryString = buildURLQuery(getValues);
  const uri = `${entity_uri}${queryString}`   
	
  try {
    const response = await httpGet(uri, headerOptions);
    return handleHttpResponse(response);
  }
  catch (error) {
    return handleHttpError(error);
  }
}

export const buildUriById = (entity_uri, id) => `${entity_uri}/${id}`;

export const getById = async (entity_uri, id) => {
  const URL = buildUriById(entity_uri, id);	

  try {
    const response = await httpGet(URL, headerOptions);
    return handleHttpResponse(response);
  }
  catch (error) {
    return handleHttpError(error);
  }
}

export const post = async (entity_uri, params) => {
  try {
    const response = await httpPost(entity_uri, params, headerOptions);
    return handleHttpResponse(response);
  }
  catch(error) {
    return handleHttpError(error);
  }
}

export const deleteById = async (entity_uri, id) => {
  const URL = buildUriById(entity_uri, id);

  try {
    const response = await httpDelete(URL, headerOptions);
    return handleHttpResponse(response);
  }
  catch (error) {
    return handleHttpError(error);
  }
}

export const put = async (entity_uri, entity) => {
  const URL = buildUriById(entity_uri, entity.id);

  try {
    const response = await httpPut(URL, entity, headerOptions);
    return handleHttpResponse(response);
  }
  catch (error) {
    return handleHttpError(error);
  }
}				