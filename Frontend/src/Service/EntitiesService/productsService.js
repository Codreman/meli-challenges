import { get, getById } from '../../Resources/middlewares/HttpMiddlewares/methodsService';
import { API_URL } from '../../utils/constants';

const getSomeProductsApi = `${API_URL}/searchSingleProduct`;
const getProductsApi = `${API_URL}/searchProducts`;

export const getSomeProduct = async(id) => {
	return getById(getSomeProductsApi, id);
}
                        
export const getProducts = async(getValues) => {
	return get(getProductsApi, getValues);
}