/**
 * @desc Dependencias
 */
import React from "react";
import { render } from "react-dom";
import { Provider } from "react-redux";
import { createBrowserHistory } from "history";
import Routes from "./Routes/Routes";
import App from './Pages/Containers/App/App';
import { config } from "dotenv";
import { store } from './Redux/store';

config();
const history = createBrowserHistory();
const storeRedux = store(history);

render(
  <Provider store={storeRedux}>
    <App history={history}>
      <Routes history={history} />
    </App>
  </Provider>,
  document.querySelector("#root")
);
