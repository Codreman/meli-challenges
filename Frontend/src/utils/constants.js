export const ServerInfo = window.location.origin;

export const COLOR_LABEL_TYPE = {
  OK: "ok",
  WARNING: "warning",
  ERROR: "error",
  PENDING: "pending"
};

export const API_URL = (() => {
  let protocol = "http://";
  let hostName = "localhost";
  let port = process.env.REACT_APP_SERVICEPORT || 8000;

  return `${protocol}${hostName}:${port}`;
})();

export const mockResponse = (code, exceptions) => ({
  code,
  exceptions
});