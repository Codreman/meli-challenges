const INITIAL_STATE = {
  products: [],
  hasQuery: false,
  search: "",
  loading: false,
  error: false,
  redirect: false,
  product: {},
  isDetail: "list",
  categories: []
};

function ProductReducer(state = INITIAL_STATE, action) {
  switch (action.type) {
    case "GET_PRODUCTS":
      return {
        ...state,
        products: action.payload
      };
    case "SEARCH":
      return {
        ...state,
        search: action.payload
      };
    case "SEARCH_PRODUCT":
      return {
        ...state,
        products: action.payload
      };
    case "SET_LOADING":
      return {
        ...state,
        loading: !state.loading
      };
    case "SET_HAS_QUERY":
      return {
        ...state,
        hasQuery: action.payload
      };
    case "GET_SOME_PRODUCT":
      return {
        ...state,
        product: action.payload
      };
    case "SET_IS_DETAIL":
      return {
        ...state,
        isDetail: action.payload
      };
    case "CLEAN_PRODUCT":
      return {
        ...state,
        product: {}
      };
    case "SET_CATEGORIES" :
      return {
        ...state,
        categories: action.payload
      }
    default:
      return {
        ...state
      };
  }
};

export default ProductReducer;
