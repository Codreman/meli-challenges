import { 
  readProduct, 
  setLoading, 
  serachItems, 
  setCategories, 
  cleanProduct } from "../Actions/productActions";
import { getProducts, getSomeProduct } from "../../Service/EntitiesService/productsService";

export function getOneProduct({id}) {
  return async (dispatch) => {
    dispatch(setLoading());
    dispatch(cleanProduct());
    const response = await getSomeProduct(id);
    if (response.code === 200) {
      dispatch(readProduct(response.response));
      dispatch(setCategories(response.response.data.categories))
    }
    dispatch(setLoading());
  }
}

export function getAllProduct(search) {
  return async (dispatch) => {
    dispatch(setLoading());
    dispatch(cleanProduct());
    const response = await getProducts({search});
    if (response.code === 200) {
      dispatch(serachItems(response.response));
      dispatch(setCategories(response.response.data.categories))
    }
    dispatch(setLoading());
  }
}








