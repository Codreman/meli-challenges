import { applyMiddleware, createStore } from "redux";
import { composeWithDevTools } from "redux-devtools-extension";
import thunk from "redux-thunk";
import logger from "redux-logger";
import { routerMiddleware, connectRouter } from "connected-react-router";
import Reducers from './Reducers/reducers';


export const store = (history) => createStore(
  connectRouter(history)(Reducers), // new root reducer with router state
  {}, //initial state
  composeWithDevTools(
    // Validamos las variables de entorno para mostrar o no el logger de redux
    process.env.REACT_APP_ENV === "development"
      ? //agregamos thunk para parabajar con logica asincrono
        applyMiddleware(thunk, logger, routerMiddleware(history))
      : applyMiddleware(thunk, routerMiddleware(history))
  )
);