const SEARCH_PRODUCT = "SEARCH_PRODUCT";
const SEARCH = "SEARCH";
const SET_LOADING = "SET_LOADING";
const GET_SOME_PRODUCT = "GET_SOME_PRODUCT";
const CLEAN_PRODUCT = "CLEAN_PRODUCT";
const SET_CATEGORIES = "SET_CATEGORIES";

export const serachItems = (payload) => ({
  type: SEARCH_PRODUCT,
  payload
});

export const navSearch = payload => ({
  type: SEARCH,
  payload
});

export const setLoading = () => ({
  type: SET_LOADING
});

export const readProduct = (payload) => ({
  type: GET_SOME_PRODUCT,
  payload
});

export const cleanProduct = () => ({
  type: CLEAN_PRODUCT
});

export const setCategories = (payload) => ({
  type: SET_CATEGORIES,
  payload
});
