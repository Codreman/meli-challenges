# Autor Christian Odreman

Challenge Frontend para Mercado Libre
![N|Meli](https://http2.mlstatic.com/ui/navigation/5.3.7/mercadolibre/logo__large_plus.png)

Dependencias base:

- Reactjs 16.12

# Estructura del proyecto

| - Frontend ----------------------------------------- | Raiz del proyecto
| - - src -------------------------------------------- | Workdir
| - - - Actions -------------------------------------- | Acciones de Redux
| - - - Assets --------------------------------------- | Imagenes y fuentes
| - - - Components ----------------------------------- | Componentes de la app
| - - - Config --------------------------------------- | Configuracion y rutas
| - - - Containers ----------------------------------- | Contenedores de la app ( Vistas )
| - - - Models --------------------------------------- | Modelo de la app
| - - - Reducers ------------------------------------- | Reducers de la app
| - - - Resources ------------------------------------ | Helpers
| - - - Utils ---------------------------------------- | constantes, utilidades varias

# iniar el proyecto

Es necesario tener Node instalado.

- npm install -G yarn
- yarn install
- yarn start
