# Challenge Mercadolibre

Challenge FullStack Mercado Libre

![N|Meli](https://http2.mlstatic.com/ui/navigation/5.3.7/mercadolibre/logo__large_plus.png)

Dependencias base:

- Nodejs 10.16

# Fontend Test pactico

Challenge Front-end Mercado Libre

Dependencias base:

- Reactjs 16+

# Estructura del proyecto

| - Frontend ------------------------------------------ | Challenge de Frontend

| - Backend ------------------------------------------ | Challenge de backend y modulo backend del Front

# Autor

Christian Giusseppe Odreman
